package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.jpg", PixmapFormat.RGB565);
        Assets.background2 = g.newPixmap("background2.jpg", PixmapFormat.RGB565);
        Assets.background3 = g.newPixmap("background3.jpg", PixmapFormat.RGB565);
        Assets.background4 = g.newPixmap("background4.jpg", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.headUp = g.newPixmap("headup.png", PixmapFormat.ARGB4444);
        Assets.headLeft = g.newPixmap("headleft.png", PixmapFormat.ARGB4444);
        Assets.headDown = g.newPixmap("headdown.png", PixmapFormat.ARGB4444);
        Assets.headRight = g.newPixmap("headright.png", PixmapFormat.ARGB4444);
        Assets.tail = g.newPixmap("tail.png", PixmapFormat.ARGB4444);
        Assets.diamond = g.newPixmap("diamond.png", PixmapFormat.ARGB4444);
        Assets.credits = g.newPixmap("credits.png", PixmapFormat.ARGB4444);
        Assets.credits_background = g.newPixmap("background_snake.jpg", PixmapFormat.ARGB4444);
        Assets.credits_text = g.newPixmap("credits_text.png", PixmapFormat.ARGB4444);
        Assets.wall = g.newPixmap("wall.png", PixmapFormat.ARGB4444);

        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.eat = game.getAudio().newSound("eat.ogg");
        Assets.xoc = game.getAudio().newSound("bitten.ogg");
        Settings.load(game.getFileIO());

        /*
        Assets.pk1 = g.newPixmap("1.png", PixmapFormat.RGB565);
        Assets.pk2 = g.newPixmap("2.png", PixmapFormat.RGB565);
        Assets.pk3 = g.newPixmap("3.png", PixmapFormat.RGB565);
        Assets.pk4 = g.newPixmap("4.png", PixmapFormat.RGB565);
        Assets.pk5 = g.newPixmap("5.png", PixmapFormat.RGB565);
        Assets.pk6 = g.newPixmap("6.png", PixmapFormat.RGB565);
        Assets.pk7 = g.newPixmap("7.png", PixmapFormat.RGB565);
        Assets.pk8 = g.newPixmap("8.png", PixmapFormat.RGB565);
        Assets.pk9 = g.newPixmap("9.png", PixmapFormat.RGB565);
        Assets.pk10 = g.newPixmap("10.png", PixmapFormat.RGB565);
        Assets.pk11 = g.newPixmap("11.png", PixmapFormat.RGB565);
        */

        /*Assets.pk12 = g.newPixmap("12.png", PixmapFormat.RGB565);
        Assets.pk13 = g.newPixmap("13.png", PixmapFormat.ARGB4444);
        Assets.pk14 = g.newPixmap("14.png", PixmapFormat.ARGB4444);
        Assets.pk15 = g.newPixmap("15.png", PixmapFormat.ARGB4444);
        Assets.pk16 = g.newPixmap("16.png", PixmapFormat.ARGB4444);
        Assets.pk17 = g.newPixmap("17.png", PixmapFormat.ARGB4444);
        Assets.pk18 = g.newPixmap("18.png", PixmapFormat.ARGB4444);
        Assets.pk19 = g.newPixmap("19.png", PixmapFormat.ARGB4444);
        Assets.pk20 = g.newPixmap("20.png", PixmapFormat.ARGB4444);
        Assets.pk21 = g.newPixmap("21.png", PixmapFormat.ARGB4444);
        Assets.pk22 = g.newPixmap("22.png", PixmapFormat.ARGB4444);
        Assets.pk23 = g.newPixmap("23.png", PixmapFormat.ARGB4444);
        Assets.pk24 = g.newPixmap("24.png", PixmapFormat.ARGB4444);
        Assets.pk25 = g.newPixmap("25.png", PixmapFormat.ARGB4444);
        Assets.pk26 = g.newPixmap("26.png", PixmapFormat.ARGB4444);*/


        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
