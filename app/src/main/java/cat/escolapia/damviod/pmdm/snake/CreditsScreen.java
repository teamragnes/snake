package cat.escolapia.damviod.pmdm.snake;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class CreditsScreen extends Screen {

    public CreditsScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {
                if (event.x < 64 && event.y > 416) {
                    Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.credits_background, 0, 0);
        g.drawPixmap(Assets.credits_text, 0, 120);
        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}

