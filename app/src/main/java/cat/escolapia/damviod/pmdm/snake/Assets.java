package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap background2;
    public static Pixmap background3;
    public static Pixmap background4;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap headUp;
    public static Pixmap headLeft;
    public static Pixmap headDown;
    public static Pixmap headRight;
    public static Pixmap tail;
    public static Pixmap diamond;
    public static Pixmap credits;
    public static Pixmap credits_background;
    public static Pixmap credits_text;
    public static Pixmap wall;

    public static Sound click;
    public static Sound eat;
    public static Sound xoc;

    /*
    public static Pixmap pk1;
    public static Pixmap pk2;
    public static Pixmap pk3;
    public static Pixmap pk4;
    public static Pixmap pk5;
    public static Pixmap pk6;
    public static Pixmap pk7;
    public static Pixmap pk8;
    public static Pixmap pk9;
    public static Pixmap pk10;
    public static Pixmap pk11;
    public static Pixmap pk12;
    public static Pixmap pk13;
    public static Pixmap pk14;
    public static Pixmap pk15;
    public static Pixmap pk16;
    public static Pixmap pk17;
    public static Pixmap pk18;
    public static Pixmap pk19;
    public static Pixmap pk20;
    public static Pixmap pk21;
    public static Pixmap pk22;
    public static Pixmap pk23;
    public static Pixmap pk24;
    public static Pixmap pk25;
    public static Pixmap pk26;
    */

}
