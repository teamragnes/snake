package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.025f;

    public Snake snake;
    public Diamond diamond[] = new Diamond[2];
    public Diamond walls[] = new Diamond[7];
    public boolean gameOver = false;;
    public int score = 0;

    public int rx;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        placeWalls();
        placeDiamond(0);
        placeDiamond(1);
    }

    private void placeDiamond(int num) {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        /*
        for (int i = 0; i < walls.length; i++){
            int wx = walls[i].x / 32;
            int wy = walls[i].y / 32;
            fields[wx][wy] = true;
        }
        */
        fields[2][3] = true;
        fields[3][3] = true;
        fields[4][3] = true;
        fields[6][6] = true;
        fields[6][7] = true;
        fields[9][9] = true;
        fields[9][10] = true;


        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;

            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        diamond[num] = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        fields[diamondX][diamondY] = true;
    }

    private void placeWalls()
    {
        walls[0] = new Diamond(2, 3 ,Diamond.TYPE_1);
        walls[1] = new Diamond(3, 3 ,Diamond.TYPE_1);
        walls[2] = new Diamond(4, 3 ,Diamond.TYPE_1);
        walls[3] = new Diamond(6, 6 ,Diamond.TYPE_1);
        walls[4] = new Diamond(6, 7 ,Diamond.TYPE_1);
        walls[5] = new Diamond(9, 9 ,Diamond.TYPE_1);
        walls[6] = new Diamond(9, 10 ,Diamond.TYPE_1);
    }

    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
            for(int i = 0; i < diamond.length; i++)
            {
                if(head.x == diamond[i].x && head.y == diamond[i].y) {
                    fields[diamond[i].x][diamond[i].y] = false;
                    score = score + 1;
                    placeDiamond(i);
                    snake.allarga();
                    if (tick > TICK_DECREMENT) {
                        tick = tick - TICK_DECREMENT;
                    }
                }
            }

            for(int i = 0; i < walls.length; i++)
            {
                if(head.x == walls[i].x && head.y == walls[i].y) {
                    gameOver = true;
                    return;
                }
            }
        }
    }
}
